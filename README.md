# Aria2Daemon
Download using Aria2c RPC daemon.


## Dependency
* Python3
* Aria2
* Openssl

## Usage

```
usage: aria2daemon.py [-h] [--version] [-q] [-l] [-L] [-p] [-s GID] [-r GID]
                      [--pause GID] [--unpause GID] [--unpause-all] [-d DIR]
                      [-o Aria2Option] [--option-file Aria2Option.json]
                      [-c rpcMethodParam,...) [rpcMethod(Param,... ...]]
                      [-g [{true,false}]] [--methods] [-D]
                      [URIs [URIs ...]]

aria2daemon 0.1 Download with aria2 rpc daemon

positional arguments:
  URIs                  Download URIs

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -q, --quit            quit/shutdown the daemon
  -l, --list            List downloads
  -L, --list-all        List all info of downloads
  -p, --purge           purges completed/error/removed downloads to free memory
  -s GID, --show GID    show given download job by gid
  -r GID, --remove GID  remove given download job by gid
  --pause GID           pause given download job by gid
  --unpause GID         unpause given download job by gid
  --unpause-all         unpause all the paused jobs
  -d DIR, --dir DIR     the directory to store downloaded file
  -o Aria2Option, --option Aria2Option
                        aria2 download option as: key=value. Option can be
                        specified multiple times.
  --option-file Aria2Option.json
                        load aria2 download options from a json file
  -c rpcMethod(Param,...) [rpcMethod(Param,...) ...], --call-method rpcMethod(Param,...) [rpcMethod(Param,...) ...]
                        make a rpc method call
  -g [{true,false}], --global-instance [{true,false}]
                        Use a global single instance of aria2 daemon to download.
                        The runtime state file is stored in
                        ~/.local/share/aria2daemon/
  --methods             list available rpc methods
  -D, --debug           debug run

```

## Files
An `aria2c` RPC daemon will be created for every download directory.

There are a few files saved into download directory:
* `aria2-cert.pem`: certification file created by `openssl` for HTTPS connection
* `aria2-keyStore.p12`: certification file used by aria2c to start secue daemon
* `aria2-runtime.json`: runtime information of the running aria2c daemon

The `--global-instance` option can be used to start a global daemon instance
that's independent from the download directory. The runtime information files
of the global daemon are stored at `~/.local/share/aria2daemon/`.

## Utilities

  * btseed-list: list information of `.torrent` files.

    ```
    usage: btseed-list [-h] [-f | -u] [-D] torrent [torrent ...]

    List BitTorrent(BT) top file name in a torrent seed file. Optionally rename
    the filename.

    positional arguments:
      torrent      *.torrent seed files

    optional arguments:
      -h, --help   show this help message and exit
      -f, --fix    Fix torrent file. Rename seed to top torrent file name
      -u, --unfix  UnFix torrent file. Rename seed to hashinfo name
      -D, --debug
    ```

