#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
"""
Make download request to aria2 RPC server
"""

from __future__ import unicode_literals, absolute_import, division
import sys
import os
import io
import logging

import ssl
import time
import json
import pathlib
import subprocess
import xmlrpc.client as rpcclient
import functools
from pprint import pprint

__version__ = "0.1"
PORT_BASE = 62999
RUNTIME_FILE = "aria2-runtime.json"
PEM_FILE = "aria2-cert.pem"
PKCS12_FILE = "aria2-keyStore.p12"
ARIA2DAEMON_DATA_DIR = "~/.local/share/aria2daemon/"

NATIVE = sys.getfilesystemencoding()

def num2human(num, kunit=1000, dec=2):
    """Convert integer to human readable units"""
    units = "KMGTP"
    # units += "EZY"
    fmt = "{:.%df}{}" % dec
    ret = str(int(num))
    num = float(num)

    for i in range(len(units) - 1, -1, -1):
        div = kunit**(i + 1)
        if (num / div) > 1:
            ret = fmt.format(num / div, units[i])
            break
    return ret

class Aria2DaemonError(Exception):
    pass

class GIDError(Aria2DaemonError):
    pass

class CertificateFailed(Aria2DaemonError):
    pass

@functools.lru_cache(maxsize=8)
def _init_rpc_server(url, cert_path):
    context = ssl.SSLContext()
    log.debug(f"_init_rpc_server cer_path: {cert_path}")
    context.load_cert_chain(cert_path)
    proxy = rpcclient.ServerProxy(url, context=context)

    return proxy

class Aria2RPC:
    def __init__(self, path, global_instance=False):
        self.path = pathlib.Path(path).absolute()
        self.global_instance = global_instance
        self.data_dir = pathlib.Path(ARIA2DAEMON_DATA_DIR).expanduser()
        self._secret = None
        self._port = None

    @property
    def runtime_path(self):
        p = self.data_dir if self.global_instance else self.path
        return p

    @property
    def cert_path(self):
        cert_path = self.runtime_path / PEM_FILE
        return cert_path

    @property
    def pkcs12_path(self):
        pkcs12_path = self.runtime_path / PKCS12_FILE
        return pkcs12_path

    @property
    def secret(self):
        if self._secret is None:
            self._load_runtime_info()
        return self._secret

    @property
    def port(self):
        if self._port is None:
            self._load_runtime_info()
        return self._port

    @property
    def rpc_url(self):
        uri = f"https://localhost:{self.port}/rpc"
        return uri

    @property
    def running(self):
        res = False
        try:
            res = self.call_method("system.listMethods")
            res = True
            log.debug(f"running(): {res}")
        except ConnectionRefusedError:
            pass
        return res

    @property
    def current_gids(self):
        res = self.call_method("tellActive", ["gid"])
        res += self.call_method("tellWaiting", 0, 9999, ["gid"])
        res += self.call_method("tellStopped", 0, 9999, ["gid"])
        gids = [r["gid"] for r in res]
        return gids

    def _load_runtime_info(self):
        runtime_path = self.runtime_path / RUNTIME_FILE
        with io.open(runtime_path) as fh:
            runtime_info = json.load(fh)
        self._secret = f"token:{runtime_info['secret']}"
        self._port = runtime_info['port']

    def init_server(self):
        try:
            options = self.getGlobalOption()
            log.debug(f"options['dir']: {options['dir']}")
            if self.runtime_path.samefile(options["dir"]):
                return
        except (ConnectionRefusedError, FileNotFoundError):
            pass
        except rpcclient.Error as e:
            log.debug(f"Error: {e}")

        # start server at available port
        port = PORT_BASE
        if self.global_instance:
            port = port - 100
        self.start_server(port)
        while True:
            uri = f"https://localhost:{port}/rpc"
            try:
                with self.rpc_server(uri) as proxy:
                    options = proxy.aria2.getGlobalOption(self.secret)
                    log.debug(f"options['dir']: {options['dir']}")
                    if self.runtime_path.samefile(options["dir"]):
                        break
            except (ConnectionRefusedError, FileNotFoundError,
                    ssl.SSLError, rpcclient.Error) as e:
                log.debug(f"init_server with port: {e}")
                port += 1
                self.start_server(port)

    def start_server(self, port):
        import uuid
        secret = str(uuid.uuid4())
        path = self.runtime_path
        if not (self.cert_path.exists() and self.pkcs12_path.exists()):
            self.gen_certificate()

        cmd = ["/usr/bin/aria2c", "--daemon", "--enable-rpc"]
        cmd.extend(["--dir", str(path)])
        cmd.extend(["--rpc-listen-port", str(port)])
        cmd.extend(["--rpc-secure=true"])
        cmd.extend(["--rpc-certificate", str(self.pkcs12_path)])
        cmd.extend(["--rpc-secret", secret])

        for f in [RUNTIME_FILE]:
            f_path = path / f
            if f_path.exists():
                f_path.unlink()
            f_path.touch(mode=0o600)

        runtime_info = {"secret": secret, "port": port}
        with io.open(path / RUNTIME_FILE, "w+") as fhw:
            json.dump(runtime_info, fhw)
        self._secret = f"token:{secret}"
        self._port = port

        log.info(f"Start aria2 server at: {self.rpc_url}...")
        log.debug(" ".join(cmd))
        subprocess.Popen(cmd)
        time.sleep(1)

    def wait_server_start(self):
        """Wait the aria2 RPC actually start to serve request"""
        timeout = 0.1
        url = self.rpc_url
        while True:
            try:
                with self.rpc_server(url) as proxy:
                    res = proxy.system.listMethods()
                    log.debug(f"wait_server_start(): {res}")
                    break
            except ConnectionRefusedError:
                pass
            time.sleep(timeout)
        return

    def gen_certificate(self):
        # create SSL cert
        self.runtime_path.mkdir(0o700, True, True)
        cmd = ["/usr/bin/openssl", "req", "-batch", "-x509",
               "-newkey", "rsa:4096", "-days", "3650", "-nodes",
               "-keyout", str(self.cert_path),
               "-out", str(self.cert_path)]
        res = subprocess.run(cmd)
        if res.returncode != 0:
            raise CertificateFailed("Failed to create certificate file")

        # convert cert to pkcs12 format
        cmd = ["/usr/bin/openssl", "pkcs12", "-export", "-passout", "pass:",
               "-in", str(self.cert_path),
               "-out", str(self.pkcs12_path)]
        res = subprocess.run(cmd)
        if res.returncode != 0:
            raise CertificateFailed("Failed to cover pkcs12 certificate file")

    def rpc_server(self, url=None):
        """Return a RPC server context manager"""
        if url is None:
            url = self.rpc_url
        return _init_rpc_server(url, self.cert_path)

    def call_method(self, method, *args):
        """Call RPC methods with the method as a string"""
        attrs = method.split(".")
        if len(attrs) == 1:
            attrs.insert(0, "aria2")
        with self.rpc_server() as proxy:
            obj = proxy
            for name in attrs:
                obj = getattr(obj, name)
            res = obj(self.secret, *args)
        return res

    def list_methods(self):
        res = self.call_method("system.listMethods")
        for m in res:
            print(f"{m}")
        return res

    def addUri(self, uris, options):
        res = self.call_method("addUri", uris, options)
        log.info(f"GID: {res}")
        return res

    def addTorrent(self, torrent, options):
        t_bytes = pathlib.Path(torrent).read_bytes()
        res = self.call_method("addTorrent", t_bytes, [], options)
        log.info(f"GID: {res}")
        return res

    def getGlobalOption(self):
        return self.call_method("getGlobalOption")


    def fetch_jobs(self, keys):
        res = []
        def sort_key(val):
            if "files" in val:
                fs = val["files"][:]
                fs.sort(key=lambda f: int(f["length"]))
                fname = fs[-1]["path"]  # biggest file
                try:
                    fname = pathlib.Path(fname).relative_to(self.path)
                except ValueError:
                    pass
                fname = str(fname).lower()
            else:
                fname = ""
            return val["status"] + val.get("seeder", "false") + fname

        for cmd in ["tellActive", "tellWaiting", "tellStopped"]:
            if cmd in ["tellActive"]:
                info = self.call_method(cmd, keys)
            else:
                info = self.call_method(cmd, 0, 99999, keys)
            info.sort(key=sort_key)
            res.extend(info)

        return res

    def seeder_gids(self, status, keys=[]):
        """Return seeding job list"""
        if status == "all":
            status_list = ["active", "waiting", "paused"]
        else:
            status_list = [status]

        need_keys = ["gid", "seeder", "status"]
        keyset = list(set(keys + need_keys))

        infos = self.fetch_jobs(keyset)
        infos = [f for f in infos if f.get("seeder", "false") == "true"]
        infos = [f for f in infos if f["status"] in status_list]
        return infos

    def list(self, full=True):
        """List download jobs"""
        keys = ["gid", "status", "files", "infoHash",
                "completedLength", "totalLength", "downloadSpeed",
                "uploadLength", "uploadSpeed", "errorMessage",
                "connections", "numSeeders", "seeder", "followedBy",
                ]
        if full:
            keys += ["bittorrent"]

        if not full and not self.global_instance:
            print(f"Dir: {self.runtime_path}")

        res = self.fetch_jobs(keys)
        for dl_info in res:
            if full:
                self.job_print_pretty(dl_info)
            else:
                self.job_print_brief(dl_info)
        print(f"\nTotal Jobs: {len(res)}\n")
        return res

    def list_seeders(self, status):
        """List seeding jobs"""
        keys = ["gid", "status", "files", "infoHash",
                "completedLength", "totalLength", "downloadSpeed",
                "uploadLength", "uploadSpeed", "errorMessage",
                "connections", "numSeeders", "seeder", "followedBy",
                ]

        res = []
        def sort_key(val):
            fs = val["files"][:]
            fs.sort(key=lambda f: int(f["length"]))
            fname = fs[-1]["path"]  # biggest file
            try:
                fname = pathlib.Path(fname).relative_to(self.path)
            except ValueError:
                pass
            fname = str(fname).lower()
            return val["status"] + val.get("seeder", "false") + fname


        infos = self.seeder_gids(status, keys)
        if not self.global_instance:
            print(f"Dir: {self.runtime_path}")

        for dl_info in infos:
            self.job_print_brief(dl_info)
        print(f"\nTotal Seeders: {len(infos)}\n")
        return res

    def purgeDownloadResult(self):
        res = self.call_method("purgeDownloadResult")
        return res

    def show_gid(self, gid):
        """Show status of a GID"""
        try:
            gid = self.real_gid(gid)
            res = self.call_method("tellStatus", gid)
            self.job_print_pretty(res)
            return res
        except (GIDError, rpcclient.Error) as e:
            log.error(e)

    def remove(self, gid):
        """Remove download job by GID"""
        try:
            gid = self.real_gid(gid)
            res = self.call_method("remove", gid)
            return res
        except (GIDError, rpcclient.Error) as e:
            log.error(e)

    def pause(self, gid):
        """Pause download job by GID"""
        try:
            gid = self.real_gid(gid)
            res = self.call_method("pause", gid)
            return res
        except (GIDError, rpcclient.Error) as e:
            log.error(e)

    def unpause(self, gid):
        """Unpause download job by GID"""
        try:
            gid = self.real_gid(gid)
            res = self.call_method("unpause", gid)
            return res
        except (GIDError, rpcclient.Error) as e:
            log.error(e)

    def unpauseAll(self):
        try:
            res = self.call_method("unpauseAll")
            return res
        except rpcclient.Error as e:
            log.error(e)

    def pause_seeders(self, status):
        """Pause the seeding jobs"""
        with self.rpc_server() as proxy:
            mc = rpcclient.MultiCall(proxy)
            for info in self.seeder_gids(status):
                mc.aria2.pause(self.secret, info["gid"])
            res = mc()
        res = tuple(res)
        return res

    def remove_seeders(self, status):
        """Remove the seeding jobs"""
        with self.rpc_server() as proxy:
            mc = rpcclient.MultiCall(proxy)
            for info in self.seeder_gids(status):
                mc.aria2.remove(self.secret, info["gid"])
            res = mc()
        res = tuple(res)
        return res

    def shutdown(self):
        """Shutdown the running aria2c RPC server for the current dir"""
        try:
            res = self.call_method("shutdown")
            (self.runtime_path / RUNTIME_FILE).unlink()
            return res
        except (FileNotFoundError, ConnectionRefusedError) as e:
            log.debug(f"shutdown error: {e}")

    def real_gid(self, gid_prefix):
        """Get the full GID from a GID prefix"""
        res = None
        gids = []
        for gid in self.current_gids:
            if gid.startswith(gid_prefix):
                gids.append(gid)

        if len(gids) > 1:
            raise GIDError(
                    f"The given GID ({gid_prefix}) is not unique: {gids}")
        elif len(gids) == 0:
            raise GIDError(f"The given GID ({gid_prefix}) is not found.")
        else:
            res = gids[0]
        return res

    def file_print_pretty(self, file_info, commonpath=None):
        """Pretty print file info"""
        nums = ["length", "completed",
                "totalLength", "completedLength", "downloadSpeed",
                "uploadLength", "uploadSpeed",
                ]
        ignores = ["bitfield", "numPieces", "pieceLength"]
        orders = ["gid", "path", "index", "selected",
                "dir", "bittorrent", "infoHash", "seeder",
                "status", "connections", "numSeeders"]

        for k in orders:
            if k not in file_info: continue
            v = file_info[k]
            if len(v) > 0 and k not in ignores:
                if (k == "path" and commonpath is not None
                        and not v.startswith("[METADATA]")):
                    v = pathlib.Path(v).relative_to(commonpath)
                    v = str(v)
                elif k == "bittorrent":
                    k = "bt name"
                    if "info" in v:
                        v = v["info"]["name"]
                    else:
                        v = ""
                print(f"{k:12s}: {v}")

        for k, v in file_info.items():
            if (k not in nums and len(v) > 0 and
                    k not in ignores and k not in orders):
                print(f"{k:12s}: {v}")

        for k in nums:
            if k not in file_info:
                continue
            v = file_info[k]
            if int(v) != 0:
                v = f"{num2human(int(v))}B ({v})"
            print(f"{k:16s}: {v}")

    def job_print_pretty(self, dl_info):
        """Pretty print job info"""
        files = dl_info["files"]
        files.sort(key=lambda x: int(x["length"]))
        commonpath = self.path
        try:
            if "bittorrent" in dl_info and len(files) > 0:
                commonpath = self.path / dl_info["bittorrent"]["info"]["name"]
        except (ValueError, KeyError):
            pass

        for finfo in files:
            self.file_print_pretty(finfo, commonpath)
            print()
        if len(files) > 1:
            print(f"Total Files: {len(files)}\n")

        del dl_info["files"]
        self.file_print_pretty(dl_info)

    def job_print_brief(self, dl_info):
        """Print brief info of a job"""
        files = dl_info["files"]
        files.sort(key=lambda x: int(x["length"]))
        file_info = files[-1].copy()
        file_info["completed"] = file_info["completedLength"]
        del dl_info["files"]
        file_info.update(dl_info)

        header = f"GID: {dl_info['gid']}"
        if "infoHash" in dl_info:
            header += f' InfoHash: {dl_info["infoHash"]}'
        print("=" * 74)
        print(header)

        if self.global_instance:
            print(f"Dir: {self.path}")
        if file_info.get("path", False):
            try:
                relative_path = pathlib.Path(file_info["path"]
                        ).relative_to(self.path)
            except ValueError:
                relative_path = file_info["path"]
        else:
            relative_path = ""
        print(f"File: {relative_path}")

        total = num2human(int(file_info["totalLength"]))
        completed = num2human(int(file_info["completedLength"]))
        upload = num2human(int(file_info["uploadLength"]))
        dspeed = num2human(int(file_info["downloadSpeed"]))
        uspeed = num2human(int(file_info["uploadSpeed"]))
        conns = dl_info["connections"]
        msg = f'Status: [{dl_info["status"]}] '
        if "numSeeders" in dl_info:
            seeds = f"/{dl_info['numSeeders']}"
            if dl_info["status"] == "complete":
                msg += f'Done: {completed}B/{total}B '
            elif dl_info.get("seeder", "false") == "true":
                msg += (f'Seeding: {upload}B/{total}B({uspeed}B/s) '
                        f'Conn: {conns}{seeds}')
            else:
                msg += (f'DL: {completed}B/{total}B({dspeed}B/s) '
                        f'UL: {upload}B({uspeed}B/s) '
                        f'Conn: {conns}{seeds}')
        elif dl_info["status"] == "complete":
            msg += f'Done: {total}B'
        else:
            msg += (f'DL: {completed}B/{total}B({dspeed}B/s) '
                    f'Conn: {conns}')
        print(msg)

        if dl_info.get("errorMessage", False):
            print(f'Error: {dl_info["errorMessage"]}')
        if dl_info.get("followedBy", False):
            print(f'FollowedBy: {dl_info["followedBy"]}')

def simple_action(arpc, args):
    """Do simple actions"""
    res = True
    if args.list:
        arpc.list(False)
    if args.list_seeders:
        arpc.list_seeders(args.list_seeders)
    elif args.list_all:
        arpc.list(True)
    elif args.show:
        arpc.show_gid(args.show)
    elif args.purge:
        res = arpc.purgeDownloadResult()
        print(res)
    elif args.remove:
        res = arpc.remove(args.remove)
        print(res)
    elif args.remove_seeders:
        res = arpc.remove_seeders(args.remove_seeders)
        print(f"{res} {len(res)}")
    elif args.pause:
        res = arpc.pause(args.pause)
        print(res)
    elif args.pause_seeders:
        res = arpc.pause_seeders(args.pause_seeders)
        print(f"{res} {len(res)}")
    elif args.unpause:
        res = arpc.unpause(args.unpause)
        print(res)
    elif args.unpause_all:
        res = arpc.unpauseAll()
        print(res)
    elif args.methods:
        arpc.list_methods()
    else:
        res = False

    return res

def process_options(args):
    """Parse aria2c options from args"""
    aria2c_options = {}
    if args.option:
        for s in args.option:
            k, _, v = s.partition("=")
            k = k.strip()
            v = v.strip()
            vo = aria2c_options.get(k, None)
            if vo is None:
                aria2c_options[k] = v
            elif isinstance(vo, list):
                vo.append(v)
            else:
                aria2c_options[k] = [aria2c_options[k], v]

    if args.option_file:
        with io.open(args.option_file) as fh:
            option_file = json.load(fh)
        aria2c_options.update(option_file)

    return aria2c_options

def process_uris(args):
    """Classify uris argument as URLs or torrents"""
    uris = args.uris
    urls = []
    torrents = []
    for u in uris:
        if u.endswith(".torrent") and not u.startswith(
                ("http://", "https://")):
            torrents.append(u)
        else:
            urls.append(u)
    return urls, torrents

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s:%(name)s:: %(message)s")
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)


setup_log()

def setup_arg_parser(appname=None):
    if not appname:
        appname = os.path.basename(sys.argv[0])
        if appname.endswith(".py"):
            appname = appname[:len(appname) - 3]

    __version_id__ = f"{appname} {__version__}"
    import argparse
    parser = argparse.ArgumentParser(
        description=f"{appname} {__version__} Download with aria2 rpc daemon",
        prefix_chars="+-")
    parser.add_argument("--version", action="version",
            version=__version_id__)

    parser.add_argument('uris', metavar='URIs', type=str, nargs="*",
            default=None, help='Download URIs')
    parser.add_argument("-q", "--quit", action="store_true",
            help="quit/shutdown the daemon")
    parser.add_argument("-l", "--list", action="store_true",
            help="List downloads")
    parser.add_argument("-L", "--list-all", action="store_true",
            help="List all info of downloads")
    parser.add_argument("--list-seeders", nargs="?", const="all",
            choices=["all", "active", "waiting", "paused"],
            help="List info of all the seeders, default: %(const)s")
    parser.add_argument("-s", "--show", metavar="GID", type=str,
            help="show given download job by GID. GID can be partial.")
    parser.add_argument("-r", "--remove", metavar="GID", type=str,
            help="remove given download job by GID. GID can be partial.")
    parser.add_argument("--remove-seeders", nargs="?", const="all",
            choices=["all", "active", "waiting", "paused"],
            help=argparse.SUPPRESS)
            #help="remove the seeding jobs")
    parser.add_argument("-p", "--purge", action="store_true",
            help="purges completed/error/removed downloads to free memory")
    parser.add_argument("--pause", metavar="GID", type=str,
            help="pause given download job by GID")
    parser.add_argument("--pause-seeders", nargs="?", const="all",
            choices=["all", "active", "waiting", "paused"],
            help="pause the seeding jobs")
    parser.add_argument("--unpause", metavar="GID", type=str,
            help="unpause given download job by GID")
    parser.add_argument("--unpause-all", action="store_true",
            help="unpause all the paused jobs")
    parser.add_argument("-d", "--dir", type=str,
            help="the directory to store downloaded file")
    parser.add_argument("-o", "--option", type=str, action="append",
            metavar="Aria2Option",
            help="aria2 download option as: key=value. "
            "Option can be specified multiple times.")
    parser.add_argument("--option-file", type=str,
            metavar="Aria2Option.json",
            help="load aria2 download options from a json file")
    parser.add_argument("-c", "--call-method", type=str, nargs="+",
            metavar="rpcMethod(Param,...)", help="make a rpc method call")
    parser.add_argument("-g", "--global-instance", choices=["true", "false"],
            nargs="?", const="true",
            help="Use a global single instance of aria2 daemon to download. "
            f"The runtime state file is stored in {ARIA2DAEMON_DATA_DIR}")
    parser.add_argument("--methods", action="store_true",
            help="list available rpc methods")
    parser.add_argument("-D", "--debug", action="store_true",
            help="debug run")

    return parser

def main():
    parser = setup_arg_parser()
    args = parser.parse_args()
    # print(args);return

    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG
    setup_log(log_level)

    if args.dir:
        path = pathlib.Path(args.dir).absolute()
    else:
        path = pathlib.Path(".").absolute()

    global_instance = False
    if args.global_instance == "true":
        global_instance = True
    elif args.global_instance == "false":
        global_instance = False

    arpc = Aria2RPC(path, global_instance)
    if args.quit:
        arpc.shutdown()
        return

    arpc.init_server()
    arpc.wait_server_start()

    aria2c_options = process_options(args)
    if "dir" not in aria2c_options:
        aria2c_options["dir"] = str(path)

    res = simple_action(arpc, args)
    if res:
        return

    if args.call_method:
        res = arpc.call_method(*args.call_method)
        if isinstance(res, str):
            print(res)
        else:
            pprint(res)
    elif args.uris:
        urls, torrents = process_uris(args)
        if len(urls) > 0:
            res = arpc.addUri(urls, aria2c_options)
        if len(torrents) > 0:
            for t in torrents:
                res = arpc.addTorrent(t, aria2c_options)

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        if log.getEffectiveLevel() == logging.DEBUG:
            import traceback
            log.debug(traceback.format_exc())
        else:
            msg = f"{e}"
            if isinstance(e, rpcclient.Error):
                msg = f"[{e.faultCode}]: {e.faultString}"
            log.error(msg)

